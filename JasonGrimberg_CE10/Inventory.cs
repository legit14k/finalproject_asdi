﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE10
{
    class Inventory
    {
        private string _productName;
        
        public Inventory(string nameParam)
        {
            ProductName = nameParam;
        }

        public string ProductName { get => _productName; set => _productName = value; }
        
        //------------------------------------------------------------------
        //public string[] AddToInventory()
        //{
        //    Inventory1[0] = "Xbox one";
        //    Inventory1[1] = "Xbox one";
        //    Inventory1[2] = "Xbox one";
        //    Inventory1[3] = "Xbox one";
        //    Inventory1[4] = "Xbox one";
        //    Inventory1[5] = "PS4";
        //    Inventory1[6] = "PS4";
        //    Inventory1[7] = "PS4";
        //    Inventory1[8] = "PS4";
        //    Inventory1[9] = "PS4";
        //    Inventory1[10] = "Laptop";
        //    Inventory1[11] = "Laptop";
        //    Inventory1[12] = "Laptop";
        //    Inventory1[13] = "Laptop";
        //    Inventory1[14] = "Laptop";
        //    Inventory1[15] = "iPhone";
        //    Inventory1[16] = "iPhone";
        //    Inventory1[17] = "iPhone";
        //    Inventory1[18] = "iPhone";
        //    Inventory1[19] = "iPhone";
        //    Inventory1[20] = "Android";
        //    Inventory1[21] = "Android";
        //    Inventory1[22] = "Android";
        //    Inventory1[23] = "Android";
        //    Inventory1[24] = "Android";
        //    Inventory1[25] = "MacBook Pro";
        //    Inventory1[26] = "MacBook Pro";
        //    Inventory1[27] = "MacBook Pro";
        //    Inventory1[28] = "MacBook Pro";
        //    Inventory1[29] = "MacBook Pro";
        //    Inventory1[30] = "Lightsaber";
        //    Inventory1[31] = "Lightsaber";
        //    Inventory1[32] = "Lightsaber";
        //    Inventory1[33] = "Lightsaber";
        //    Inventory1[34] = "Lightsaber";
        //    Inventory1[35] = "Monitor";
        //    Inventory1[36] = "Monitor";
        //    Inventory1[37] = "Monitor";
        //    Inventory1[38] = "Monitor";
        //    Inventory1[39] = "Monitor";
        //    Inventory1[40] = "Speakers";
        //    Inventory1[41] = "Speakers";
        //    Inventory1[42] = "Speakers";
        //    Inventory1[43] = "Speakers";
        //    Inventory1[44] = "Speakers";
        //    Inventory1[45] = "Gumball";
        //    Inventory1[46] = "Gumball";
        //    Inventory1[47] = "Gumball";
        //    Inventory1[48] = "Gumball";
        //    Inventory1[49] = "Gumball";
        //    Console.WriteLine(Inventory1[22]);
        //    Console.ReadKey();
        //    return Inventory1;

        //}

        //------------------------------------------------------------------
        public void DisplayAllItemsInventory()
        {
            Console.WriteLine($"{ProductName}");
        }
    }

    
}
