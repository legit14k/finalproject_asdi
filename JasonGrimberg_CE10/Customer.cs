﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE10
{
    class Customer : Person
    {
        private string _currentCustomer;
        List<Person> _person;
        List<Inventory> _inventory;

        public Customer(string nameParam, int ageParam) : base(nameParam, ageParam)
        {
            Person = new List<Person>();
            _inventory = new List<Inventory>();
        }
        
        public List<Person> Person { get => _person; set => _person = value; }
        public string CurrentCustomer { get => _currentCustomer; set => _currentCustomer = value; }
        internal List<Inventory> Inventory1 { get => _inventory; set => _inventory = value; }

        public string NewCustomer(string newCustomer)
        {
            CurrentCustomer = newCustomer;
            return CurrentCustomer;
        }

        //------------------------------------------------------------------
        public void AddOrUpdateDict(Dictionary<string, List<string>> dict, string key, List<string> value)
        {
            if (dict.ContainsKey(key))
            {
                dict[key] = value;
            }
            else
            {
                dict.Add(key, value);
            }

            Console.WriteLine("Key: " + key + " Value: " + dict[key]);

            Console.ReadKey();
        }

        //------------------------------------------------------------------
        public void AddCustomer()
        {
            string tempName;
            string ageHolder;
            int tempAge = 0;

            Console.Write("New customer's name?: ");
            tempName = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(tempName))
            {
                Console.Write("Please do not leave blank.\r\nNew customer's name?: ");

                tempName = Console.ReadLine();
            }

            Console.Write("New customer's age?: ");
            ageHolder = Console.ReadLine();

            while(!int.TryParse(ageHolder, out tempAge))
            {
                Console.Write("Please enter a number.\r\nNew customer's age?: ");
                ageHolder = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(ageHolder))
                {
                    Console.Write("Please do not leave blank.\r\nNew customer's age?: ");
                    ageHolder = Console.ReadLine();
                }
            }

            Person person = new Person(tempName, tempAge);
            Person.Add(person);
            
            Console.WriteLine($"Added: {tempName} with an age of {tempAge} to customers ");
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        //------------------------------------------------------------------
        public void DisplayCustomers()
        {
            Console.WriteLine("==================================");
            Console.WriteLine("All customers");
            Console.WriteLine("==================================\n");
            foreach (Person p in _person)
            {
                p.DisplayAllCustomers();
            }
            Console.WriteLine("==================================");
            WaitForAnyKey();
        }

        //------------------------------------------------------------------
        public void AddToInventory()
        {
            string tempName1 = "Xbox One";
            string tempName2 = "PS4";
            string tempName3 = "Laptop";
            string tempName4 = "iPhone";
            string tempName5 = "Android";
            string tempName6 = "MacBook Pro";
            string tempName7 = "Lightsaber";
            string tempName8 = "Monitor";
            string tempName9 = "Speakers";
            string tempName10 = "Gumball";

            Inventory new1 = new Inventory(tempName1);
            Inventory1.Add(new1);
            Inventory1.Add(new1);
            Inventory1.Add(new1);
            Inventory1.Add(new1);
            Inventory1.Add(new1);

            Inventory new2 = new Inventory(tempName2);
            Inventory1.Add(new2);
            Inventory1.Add(new2);
            Inventory1.Add(new2);
            Inventory1.Add(new2);
            Inventory1.Add(new2);

            Inventory new3 = new Inventory(tempName3);
            Inventory1.Add(new3);
            Inventory1.Add(new3);
            Inventory1.Add(new3);
            Inventory1.Add(new3);
            Inventory1.Add(new3);

            Inventory new4 = new Inventory(tempName4);
            Inventory1.Add(new4);
            Inventory1.Add(new4);
            Inventory1.Add(new4);
            Inventory1.Add(new4);
            Inventory1.Add(new4);

            Inventory new5 = new Inventory(tempName5);
            Inventory1.Add(new5);
            Inventory1.Add(new5);
            Inventory1.Add(new5);
            Inventory1.Add(new5);
            Inventory1.Add(new5);

            Inventory new6 = new Inventory(tempName6);
            Inventory1.Add(new6);
            Inventory1.Add(new6);
            Inventory1.Add(new6);
            Inventory1.Add(new6);
            Inventory1.Add(new6);

            Inventory new7 = new Inventory(tempName7);
            Inventory1.Add(new7);
            Inventory1.Add(new7);
            Inventory1.Add(new7);
            Inventory1.Add(new7);
            Inventory1.Add(new7);

            Inventory new8 = new Inventory(tempName8);
            Inventory1.Add(new8);
            Inventory1.Add(new8);
            Inventory1.Add(new8);
            Inventory1.Add(new8);
            Inventory1.Add(new8);

            Inventory new9 = new Inventory(tempName9);
            Inventory1.Add(new9);
            Inventory1.Add(new9);
            Inventory1.Add(new9);
            Inventory1.Add(new9);
            Inventory1.Add(new9);

            Inventory new10 = new Inventory(tempName10);
            Inventory1.Add(new10);
            Inventory1.Add(new10);
            Inventory1.Add(new10);
            Inventory1.Add(new10);
            Inventory1.Add(new10);

        }

        //------------------------------------------------------------------
        public void DisplayCurrentCustomer(string currentCust)
        {
            int tempNumb = 1;
            Console.WriteLine("==================================");
            Console.WriteLine("Current Customer: {0}", _currentCustomer);
            Console.WriteLine("==================================");
            Console.WriteLine("Please choose a customer to switch to:\n");
            foreach (Person p in _person)
            {
                Console.Write(@"{0}) ",tempNumb);
                p.DisplayAllCustomers();
                tempNumb = tempNumb + 1;
            }
            
            WaitForAnyKey();
        }

        //------------------------------------------------------------------
        public void DisplayCurrentCart()
        {
            Console.WriteLine("====================");
            Console.WriteLine("Current Cart");
            Console.WriteLine("====================\n");

            WaitForAnyKey();
        }

        //------------------------------------------------------------------
        public void ViewInventory()
        {
            
            Console.WriteLine("=========================");
            Console.WriteLine("Current Inventory");
            Console.WriteLine("=========================\n");
            foreach (Inventory i in Inventory1)
            {
                i.DisplayAllItemsInventory();
            }
            Console.WriteLine("=========================");

            WaitForAnyKey();
        }
        
        //------------------------------------------------------------------
        public void AddItemToCurrentCart()
        {
            Console.WriteLine("Select an item to add to cart: ");

            WaitForAnyKey();
        }

        //------------------------------------------------------------------
        public void RemoveItemFromCurrentCart()
        {
            Console.WriteLine("Please choose what item to remove from cart: ");

            WaitForAnyKey();
        }

        //------------------------------------------------------------------
        public void CompletePurchaseCurrent()
        {
            Console.WriteLine("==========================");
            Console.WriteLine("Your completed purchase");
            Console.WriteLine("==========================\n");

            Console.WriteLine("==========================");
            WaitForAnyKey();
        }

        //------------------------------------------------------------------
        static void WaitForAnyKey()
        {
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey(true);
        }

    }
}
