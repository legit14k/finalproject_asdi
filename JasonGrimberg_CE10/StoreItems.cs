﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE10
{
    class StoreItems
    {
        private string _productName;
        private string _description;
        private decimal _price;

        public string ProductName { get => _productName; set => _productName = value; }
        public string Description { get => _description; set => _description = value; }
        public decimal Price { get => _price; set => _price = value; }

        public StoreItems(string nameParam, string descriptionParam, decimal priceParam)
        {
            NewItemName(nameParam);
            NewDescription(descriptionParam);
            NewPrice(priceParam);
        }

        public string NewItemName(string newItemName)
        {
            ProductName = newItemName;
            return ProductName;
        }

        public string NewDescription(string newDescription)
        {
            Description = newDescription;
            return Description;
        }

        public decimal NewPrice(decimal newPrice)
        {
            Price = newPrice;
            return Price;
        }
    }
}
