﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE10
{
    class Person
    {
        private string _name;
        private int _age;

        public Person(string nameParam, int ageParam)
        {
            PersonName(nameParam);
            PersonAge(ageParam);
        }

        public string Name { get => _name; set => _name = value; }
        public int Age { get => _age; set => _age = value; }

        public string PersonName(string personName)
        {
            Name = personName;
            return Name;
        }

        public int PersonAge(int personAge)
        {
            Age = personAge;
            return Age;
        }

        public void DisplayAllCustomers()
        {
            Console.WriteLine($"Name: {Name}\nAge: {Age}\n");
        }
    }
}
