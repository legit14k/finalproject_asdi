﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE10
{
    class Program
    {
        static void Main(string[] args)
        {
            var dictionary = new Dictionary<string, List<string>>();

            // Change title of the console
            Console.Title = "Store Inventory Management System";

            // Current customer
            Customer currentCustomer = null;

            // Current inventory
            Inventory currentInventory = null;

            // Running
            bool running = true;
            string userInput = "";

            while (running)
            {
                // Change title of the console
                Console.Title = "Store Inventory Management System - Main Menu";

                // Current customer
                if (currentCustomer != null)
                {
                    Console.Clear();
                    Console.WriteLine("-----------------------------------------");
                    Console.WriteLine("Current customer: {0}", currentCustomer.CurrentCustomer);
                    Console.WriteLine("-----------------------------------------");
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("-----------------------------------------");
                    Console.WriteLine("Current customer: No current customer");
                    Console.WriteLine("-----------------------------------------");
                }

                PrintMenu();
                Console.Write("\nEnter Command: ");

                userInput = Console.ReadLine().ToLower();

                switch (userInput)
                {
                    case "1":
                    case "select current shopper":
                        {
                            if (currentCustomer != null)
                            {
                                currentCustomer.DisplayCurrentCustomer(currentCustomer.Name);
                            }
                            else
                            {
                                Console.WriteLine("Please create a customer first.");
                                WaitForAnyKey();
                            }
                            break;
                        }

                    case "2":
                    case "add shopper":
                        {
                            if (currentCustomer != null)
                            {
                                bool trueOrFalse = true;
                                currentCustomer.AddCustomer();
                                Console.Write("\nWould you like {0} to be the current customer? (y/n): ", currentCustomer.Name);
                                string verifyYesOrNo = Console.ReadLine().ToLower();
                                while (trueOrFalse)
                                {
                                    switch (verifyYesOrNo)
                                    {
                                        case "y":
                                        case "yes":
                                            {
                                                currentCustomer.DisplayCurrentCustomer(currentCustomer.Name);
                                                trueOrFalse = false;
                                                break;
                                            }

                                        case "n":
                                        case "no":
                                            {
                                                trueOrFalse = false;
                                                break;
                                            }

                                        default:
                                            {
                                                Console.WriteLine("Invalid command: " + userInput);
                                                WaitForAnyKey();
                                                trueOrFalse = false;
                                                break;
                                            }
                                    }
                                }
                            }
                            else
                            {
                                
                                if (currentInventory == null)
                                {
                                    bool trueOrFalse = true;
                                    currentCustomer = new Customer("", 0);
                                    currentCustomer.AddCustomer();
                                    currentInventory = new Inventory("");
                                    currentCustomer.AddToInventory();
                                    Console.Write("Would you like {0} to be the current customer? (y/n): ", currentCustomer.Name);
                                    string verifyYesOrNo = Console.ReadLine().ToLower();
                                    while (trueOrFalse)
                                    {
                                        switch (verifyYesOrNo)
                                        {
                                            case "y":
                                            case "yes":
                                                {
                                                    currentCustomer.DisplayCurrentCustomer(currentCustomer.Name);
                                                    trueOrFalse = false;
                                                    break;
                                                }
                                                

                                            case "n":
                                            case "no":
                                                {
                                                    trueOrFalse = false;
                                                    break;
                                                }

                                            default:
                                                {
                                                    Console.WriteLine("Invalid command: " + userInput);
                                                    WaitForAnyKey();
                                                    trueOrFalse = false;
                                                    break;
                                                }
                                        }
                                    }
                                    
                                }
                            }
                            break;
                        }

                    case "3":
                    case "display shopper(s)":
                    case "display shopper":
                    case "display shoppers":
                        {
                            if(currentCustomer != null)
                            {
                                currentCustomer.DisplayCustomers();
                            }
                            else
                            {
                                Console.WriteLine("Please create customers to display.");
                                WaitForAnyKey();
                            }
                            break;
                        }

                    case "4":
                    case "view store inventory":
                        {
                            if (currentCustomer != null)
                            {

                                currentCustomer.ViewInventory();
                            }
                            else
                            {
                                Console.WriteLine("Please create a customer to view items.");
                                WaitForAnyKey();
                            }
                            break;
                        }

                    case "5":
                    case "view cart":
                        {
                            if (currentCustomer != null)
                            {
                                currentCustomer.DisplayCurrentCart();
                            }
                            else
                            {
                                Console.WriteLine("Please create a customer first to view cart");
                                WaitForAnyKey();
                            }
                            break;
                        }

                    case "6":
                    case "add item to cart":
                        {
                            if(currentCustomer != null)
                            {
                                currentCustomer.AddItemToCurrentCart();
                            }
                            else
                            {
                                Console.WriteLine("Please create a customer to add itmes to your cart.");
                                WaitForAnyKey();
                            }
                            break;
                        }

                    case "7":
                    case "remove item from cart":
                        {
                            if(currentCustomer != null)
                            {
                                currentCustomer.RemoveItemFromCurrentCart();
                            }
                            else
                            {
                                Console.WriteLine("Please add a customer first.");
                                WaitForAnyKey();
                            }
                            break;
                        }

                    case "8":
                    case "complete purchase":
                        {
                            if (currentCustomer != null)
                            {
                                currentCustomer.CompletePurchaseCurrent();
                            }
                            else
                            {
                                Console.WriteLine("Please add a customer first.");
                                WaitForAnyKey();
                            }
                            break;
                        }

                    case "9":
                    case "quit":
                    case "exit":
                        {
                            return;
                        }

                    default:
                        {
                            Console.WriteLine("Invalid command: " + userInput);
                            WaitForAnyKey();
                            break;
                        }
                }
            }
        }

        //------------------------------------------------------------------
        static void PrintMenu()
        {
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("Commands:                 ");
            Console.WriteLine("1) Select current shopper ");
            Console.WriteLine("2) Add shopper            ");
            Console.WriteLine("3) Display shopper(s)     ");
            Console.WriteLine("4) View store inventory   ");
            Console.WriteLine("5) View Cart              ");
            Console.WriteLine("6) Add item to cart       ");
            Console.WriteLine("7) Remove item from cart  ");
            Console.WriteLine("8) Complete purchase      ");
            Console.WriteLine("9) Quit / Exit            ");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++");
        }

        //------------------------------------------------------------------
        static void WaitForAnyKey()
        {
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey(true);
        }
    }

}